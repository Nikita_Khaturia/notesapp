package com.zanjo.lenovo.rain.rain.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.zanjo.lenovo.rain.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView chatTV,noteTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        registerViews();
        initializeVariables();
        attachListerners();
    }

    private void attachListerners() {

        noteTV.setOnClickListener(this);
        chatTV.setOnClickListener(this);
    }

    private void initializeVariables() {
    }

    private void registerViews() {
        noteTV = findViewById(R.id.noteTV);
        chatTV = findViewById(R.id.chatTV);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.noteTV:
                startActivity(new Intent(this, AllNotesActivity.class));
                break;
            case R.id.chatTV:
                break;
        }

    }
}
