package com.zanjo.lenovo.rain.rain.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.zanjo.lenovo.rain.R;
import com.zanjo.lenovo.rain.rain.adapter.AllNotesAdapter;
import com.zanjo.lenovo.rain.rain.model.NoteModel;
import com.zanjo.lenovo.rain.rain.utilities.DataHelper;
import com.zanjo.lenovo.rain.rain.utilities.ItemOffsetDecoration;

import java.util.ArrayList;

public class AllNotesActivity extends AppCompatActivity {

    private static final String TAG = "AllNotesActivity" ;
    private ImageView addTextNoteBackIV;
    private RecyclerView notesRV;
    private Toolbar toolbar;
    private FloatingActionButton addNewNote;
    private TextView noNotesTV;
    private DataHelper dataHelper;
    private ArrayList<NoteModel> noteModels;
    private AllNotesAdapter allNotesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_notes);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        registerViews();
        initialiseVariables();
        attachListeners();
        getNotesFromDB(1);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        noteModels.clear();
        getNotesFromDB(2);

    }

    private void getNotesFromDB(int count) {
        if (dataHelper.CheckDatabase(this)) {
            if (dataHelper.getAllNotes().size() > 0) {
                notesRV.setVisibility(View.VISIBLE);
                noNotesTV.setVisibility(View.GONE);
                noteModels = dataHelper.getAllNotes();
                Toast.makeText(this, "size "+noteModels.size(), Toast.LENGTH_SHORT).show();

                for (int i=0; i<noteModels.size();i++){
                    Log.e(TAG,"title "+noteModels.get(i).getNoteTittle()+" id "+noteModels.get(i).getNoteId());
                }

                allNotesAdapter = new AllNotesAdapter(noteModels, AllNotesActivity.this);
                RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(AllNotesActivity.this,2);

                if (count ==1){
                    ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(this, R.dimen.default_padding);
                    notesRV.addItemDecoration(itemDecoration);
                }else {
                    ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(this, R.dimen.default__);
                    notesRV.addItemDecoration(itemDecoration);
                }


                notesRV.setLayoutManager(mLayoutManager);

                notesRV.setAdapter(allNotesAdapter);
                allNotesAdapter.notifyDataSetChanged();
            }else {
                notesRV.setVisibility(View.GONE);
                noNotesTV.setVisibility(View.VISIBLE);
            }
        } else {
            notesRV.setVisibility(View.GONE);
            noNotesTV.setVisibility(View.VISIBLE);
        }
    }

    private void attachListeners() {
        addNewNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AllNotesActivity.this, NotesActivity.class);
                intent.putExtra("isNoteNew", 1);
                startActivity(intent);
            }
        });

        addTextNoteBackIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void initialiseVariables() {
        dataHelper = new DataHelper(this);
        noteModels = new ArrayList<>();


    }

    private void registerViews() {
        addNewNote = (FloatingActionButton) findViewById(R.id.addNewNote);
        noNotesTV = findViewById(R.id.noNotesTV);
        notesRV = findViewById(R.id.notesRV);
        addTextNoteBackIV = (ImageView) findViewById(R.id.addTextNoteBackIV);
    }

}
