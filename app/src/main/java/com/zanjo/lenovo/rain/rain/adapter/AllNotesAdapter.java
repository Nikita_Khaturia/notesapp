package com.zanjo.lenovo.rain.rain.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.zanjo.lenovo.rain.R;
import com.zanjo.lenovo.rain.rain.activity.NotesActivity;
import com.zanjo.lenovo.rain.rain.model.NoteModel;
import com.zanjo.lenovo.rain.rain.utilities.DataHelper;

import java.util.ArrayList;


/**
 * Created by xyz on 9/6/2017.
 */

public class AllNotesAdapter extends RecyclerView.Adapter<AllNotesAdapter.NotesHolder> {
    private ArrayList<NoteModel> noteModels;
    Context context;

    public AllNotesAdapter(ArrayList<NoteModel> noteModels, Context context) {
        this.noteModels = noteModels;
        this.context = context;
    }

    @Override
    public NotesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.notes_item_row, parent, false);
        return new NotesHolder(itemView);
    }

    @Override
    public void onBindViewHolder(NotesHolder holder, final int position) {

        final NoteModel noteModel = noteModels.get(position);

        if (noteModel.getNoteTittle() != null && !noteModel.getNoteTittle().isEmpty()){
            holder.titleTV.setVisibility(View.VISIBLE);
            holder.titleTV.setText(noteModel.getNoteTittle());
        }else {
            holder.titleTV.setVisibility(View.GONE);
        }

        holder.contentTV.setText(noteModel.getNoteContent());

        Log.e("NoteId in Adapter", String.valueOf(noteModel.getNoteId()));


        holder.noteCV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, NotesActivity.class);
                intent.putExtra("isNewNote", 0);
                intent.putExtra("noteId", noteModel.getNoteId());
                context.startActivity(intent);
            }
        });


        holder.noteCV.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.delete_note_popup);
                Button yesBtn = (Button) dialog.findViewById(R.id.yesBtn);
                Button noBtn = (Button) dialog.findViewById(R.id.noBtn);
                yesBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (new DataHelper(context).delete(noteModel.getNoteId())) {
                            Toast.makeText(context, "Deleted", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, "Not Deleted", Toast.LENGTH_SHORT).show();
                        }
                        dialog.dismiss();
                        try {
                            noteModels.remove(position);
                            notifyDataSetChanged();

                        }catch (Exception e){
//                            e.getMessage();
                        }
                    }
                });
                noBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return noteModels.size();
    }

    public class NotesHolder extends RecyclerView.ViewHolder {

        TextView titleTV, contentTV;
        CardView noteCV;

        public NotesHolder(View itemView) {
            super(itemView);
            titleTV = (TextView) itemView.findViewById(R.id.titleTV);
            contentTV = (TextView) itemView.findViewById(R.id.contentTV);
            noteCV = (CardView) itemView.findViewById(R.id.noteCV);
        }
    }

}
