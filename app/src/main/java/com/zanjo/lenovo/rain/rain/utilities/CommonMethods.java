package com.zanjo.lenovo.rain.rain.utilities;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
/**
 * Created by xyz on 9/15/2017.
 */

public class CommonMethods {



    public String getCurrentDateTime() {
        StringBuilder stringBuilder = new StringBuilder();
        DateFormat dateFormat = new SimpleDateFormat("MMM");
        Date date = new Date();
        stringBuilder.append(dateFormat.format(date) + " ");
        //monthTV.setText("" + dateFormat.format(date));
        DateFormat day = new SimpleDateFormat("d");
        //dayTV.setText("" + day.format(date));
        stringBuilder.append(day.format(date) + ", ");
        DateFormat year = new SimpleDateFormat("y");
        // yearTV.setText("" + year.format(date));
        stringBuilder.append(year.format(date));
        return stringBuilder.toString();
    }


}




