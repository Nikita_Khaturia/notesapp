package com.zanjo.lenovo.rain.rain.model;

/**
 * Created by Lenovo on 15-Nov-18.
 */

public class NoteModel {

    public int getNoteId() {
        return noteId;
    }

    public void setNoteId(int noteId) {
        this.noteId = noteId;
    }

    public String getNoteTittle() {
        return noteTittle;
    }

    public void setNoteTittle(String noteTittle) {
        this.noteTittle = noteTittle;
    }

    public String getNoteContent() {
        return noteContent;
    }

    public void setNoteContent(String noteContent) {
        this.noteContent = noteContent;
    }

    int noteId;
    String noteTittle;

    public NoteModel(int noteId, String noteTittle, String noteContent) {
        this.noteId = noteId;
        this.noteTittle = noteTittle;
        this.noteContent = noteContent;
    }

    public NoteModel(String noteTittle, String noteContent) {

        this.noteTittle = noteTittle;
        this.noteContent = noteContent;
    }

    public NoteModel(){

    }

    String noteContent;
}
