package com.zanjo.lenovo.rain.rain.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.zanjo.lenovo.rain.R;
import com.zanjo.lenovo.rain.rain.model.NoteModel;
import com.zanjo.lenovo.rain.rain.utilities.CommonMethods;
import com.zanjo.lenovo.rain.rain.utilities.DataHelper;

import org.json.JSONException;

import java.io.IOException;

public class NotesActivity extends AppCompatActivity {

    private ImageView addTextNoteBackIV, addNoteDoneIV;
    private EditText tittleET, contentET;
    private Toolbar toolbar;
    private TextView dateTimeTV;
    private DataHelper dataHelper;
    private NoteModel noteModel;
    private static String TAG = "NotesActivity";
    private Intent intent = null;
    private int isNewNote = 1, noteId = 0;
    private CommonMethods commonMethods;
    private String noteTitle, noteContent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notes);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        registerViews();
        initialiseVariables();
        attachListeners();

        getDataFromIntent();

        dateTimeTV.setText(commonMethods.getCurrentDateTime());


    }

    private void attachListeners() {

        addTextNoteBackIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        addNoteDoneIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                noteContent = contentET.getText().toString().trim();
                noteTitle = tittleET.getText().toString().trim();
                Log.e(TAG, "isNewNote after addNoteDoneIV click " + isNewNote);


                if (isNewNote == 0) {
                    // user has come here to edit already created note
                    if (noteContent != null && !noteContent.isEmpty()){
                        editNoteInDb();
                    }else {
//                        finish();
                    }

                } else {
                    // this is a new text note
                    if (noteContent != null && !noteContent.isEmpty()){
                        createNoteInDB();
                    }else {
                        finish();
                    }

                }
            }
        });


    }

    private void editNoteInDb() {

        if (dataHelper.editNote(noteId, noteTitle, noteContent)) {
            Toast.makeText(this, "Note Edited", Toast.LENGTH_SHORT).show();
            finish();
        }else {
            Toast.makeText(this, "Note not Edited", Toast.LENGTH_SHORT).show();
        }

    }

    private void createNoteInDB() {

        NoteModel noteModel = new NoteModel(noteTitle, noteContent);
        if (dataHelper.addNote(noteModel)) {
            Toast.makeText(this, "Note Added", Toast.LENGTH_SHORT).show();
            finish();
        } else {
            Toast.makeText(this, "Note not Added", Toast.LENGTH_SHORT).show();
        }
    }


    private void initialiseVariables() {
        commonMethods = new CommonMethods();
        dataHelper = new DataHelper(this);
        noteModel = new NoteModel();
    }

    private void registerViews() {

        addNoteDoneIV = (ImageView) findViewById(R.id.addNoteDoneIV);
        addTextNoteBackIV = (ImageView) findViewById(R.id.addTextNoteBackIV);
        tittleET = (EditText) findViewById(R.id.tittleET);
        contentET = (EditText) findViewById(R.id.contentET);
        dateTimeTV = (TextView) findViewById(R.id.dateTimeTV);

    }

    private void getDataFromIntent() {
        intent = getIntent();
        if (intent != null && intent.getExtras() != null) {

            isNewNote = intent.getIntExtra("isNewNote", 1);
            Log.e(TAG, "isNewNote in intent " + isNewNote);

            // if isNewNote == 1 user is creating new Note

            if (isNewNote == 0) {
                // user has come here to edit already created note
                noteId  = intent.getIntExtra("noteId", 1);
                noteModel = dataHelper.getSingleNote(noteId);
                if (noteModel != null){
                    tittleET.setText(noteModel.getNoteTittle());
                    contentET.setText(noteModel.getNoteContent());
                }
            }
        }
    }
}
