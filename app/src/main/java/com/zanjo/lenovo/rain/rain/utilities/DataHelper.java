package com.zanjo.lenovo.rain.rain.utilities;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.zanjo.lenovo.rain.rain.model.NoteModel;

import java.io.File;
import java.util.ArrayList;
import java.util.Random;


public class DataHelper extends SQLiteOpenHelper {

    private Random random = new Random();
    public static final String DBNAME = "sqlite.db";
    public static final int DBVersion = 1;

    SQLiteDatabase db;

    public DataHelper(Context context) {
        super(context, DBNAME, null, DBVersion);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub               UNIQUE


        db.execSQL("create table NOTES(srno integer primary key autoincrement, tittle text,content text)");

        Log.e("DB", "created");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
    }

    public boolean addNote(NoteModel noteModel) {
        SQLiteDatabase db = getWritableDatabase();
        boolean result = true;
        try {

            ContentValues cv = new ContentValues();
            cv.put("tittle", noteModel.getNoteTittle());
            cv.put("content", noteModel.getNoteContent());

            db.insert("NOTES", null, cv);

        } catch (SQLiteException ex) {
            Log.e("error ", ex.toString());
            result = false;
        }

        db.close();
        return result;
    }


    public ArrayList<NoteModel> getAllNotes() {
        ArrayList<NoteModel> stList = new ArrayList<NoteModel>();

        SQLiteDatabase db = getWritableDatabase();
        String query = "select * from NOTES  ORDER BY srno desc;";

        Cursor cur = db.rawQuery(query, null);

        Log.e("rows notes", cur.getCount() + "");

        while (cur.moveToNext()) {

            int srno = cur.getInt(0);
            String tittle = cur.getString(1);
            String content = cur.getString(2);

            NoteModel noteModel = new NoteModel(srno, tittle,
                    content);
            stList.add(noteModel);
        }
        cur.close();
        db.close();
        return stList;
    }


    public NoteModel getSingleNote(int id) {
        SQLiteDatabase db = getWritableDatabase();
        String query = "select * from NOTES where srno='" + id + "';";

        NoteModel noteModel = new NoteModel();
        Cursor cur = null;

        cur = db.rawQuery(query, null);
        if (cur.getCount() > 0) {
            cur.moveToFirst();

            int srno = cur.getInt(0);
            String tittle = cur.getString(1);
            String content = cur.getString(2);


            noteModel = new NoteModel(srno, tittle,
                    content);
        }
        cur.close();
        db.close();

        return noteModel;

    }

    public boolean CheckDatabase(Context context) {

        boolean b = false;

        File database = context.getDatabasePath("sqlite.db");

        if (!database.exists()) {
            b = false;
            // Database does not exist so copy it from assets here

            Log.e("Database", "Not Found");

        } else {
            b = true;
            Log.e("Database", "Found");
        }

        return b;
    }

    public boolean editNote(int noteId, String title, String content) {
        SQLiteDatabase db = getWritableDatabase();
        try {
            ContentValues newValues = new ContentValues();

            newValues.put("tittle", title);
            newValues.put("content", content);

            db.update("NOTES", newValues, "srno=" + noteId, null);
            //Log.e("error", "editNote");
            return true;

        } catch (Exception e) {
            Log.e("error db", "editNote");
            return false;
        }
    }


    public boolean delete(int noteId) {
        SQLiteDatabase db = getWritableDatabase();
        try {
            ContentValues newValues = new ContentValues();

            db.delete("NOTES", "srno=" + noteId, null);
            //Log.e("error", "editNote");
            return true;

        } catch (Exception e) {
            Log.e("error db", "editNote");
            return false;
        }
    }


}
